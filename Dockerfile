FROM lhcbdev/centos7-build

RUN yum install -y openssh-server java-1.8.0-openjdk-devel && yum clean all && mkdir -p /ccache

ENV CMTCONFIG=x86_64-centos7-gcc62-opt \
    MYSITEROOT=/cvmfs/lhcb.cern.ch/lib \
    LCG_hostos=x86_64-centos7

ENV LHCBRELEASES=${MYSITEROOT}/lhcb \
    LCG_external_area=${MYSITEROOT}/lcg/external \
    LCG_release_area=${MYSITEROOT}/lcg/releases:${MYSITEROOT}/lcg/app/releases:${MYSITEROOT}/lcg/external

ENV LBSCRIPTS_ROOT=${LHCBRELEASES}/LBSCRIPTS/dev

ENV LBCONFIGURATIONROOT=${LBSCRIPTS_ROOT}/LbConfiguration \
    LBRELEASEROOT=${LBSCRIPTS_ROOT}/LbRelease \
    LBUTILSROOT=${LBSCRIPTS_ROOT}/LbUtils

ENV CMAKE_PREFIX_PATH=${LBRELEASEROOT}/data/DataPkgEnvs:${LBUTILSROOT}/cmake:/cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2017/vtune_amplifier_xe \
    CMTPROJECTPATH=${LHCBRELEASES}:${LCG_release_area}

ENV PATH=${LBSCRIPTS_ROOT}/InstallArea/scripts:${MYSITEROOT}/lcg/releases/LCG_87/Python/2.7.10/${CMTCONFIG}/bin:${MYSITEROOT}/contrib/ninja/1.4.0/x86_64-slc6:${MYSITEROOT}/contrib/CMake/3.9.0/Linux-x86_64/bin:${MYSITEROOT}/contrib/CMT/v1r20p20090520/Linux-x86_64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
    PYTHONPATH=${LBSCRIPTS_ROOT}/InstallArea/python

ENV CCACHE_DIR=/ccache \
    CCACHE_CPP2=1 \
    CMAKEFLAGS=-DCMAKE_USE_CCACHE=1

RUN pip install autopep8 argparse

EXPOSE 22 4401 4403 4411 8000
USER root
WORKDIR /projects

CMD python -m SimpleHTTPServer >& /dev/null
